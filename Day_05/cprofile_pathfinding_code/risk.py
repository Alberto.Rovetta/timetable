import heapq as hq
from collections import defaultdict as ddict
import numpy as np
from time import sleep
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

with open('input.txt') as f:
    data = f.readlines()

# data = """\
# 1163751742
# 1381373672
# 2136511328
# 3694931569
# 7463417111
# 1319128137
# 1359912421
# 3125421639
# 1293138521
# 2311944581""".split('\n')

data = [[int(x) for x in line.strip()] for line in data]

data = np.array(data)
data = np.concatenate([data, data+1, data+2, data+3, data+4],axis=1)        
data[data > 9] -= 9
data = np.concatenate([data, data+1, data+2, data+3, data+4],axis=0)        
data[data > 9] -= 9

#data[data < 7] = 1

display_orig = np.concatenate([data[:,:,None],data[:,:,None],data[:,:,None]],axis=2, dtype='float')
display_orig /= 10
display = display_orig.copy()
#print(display)
#exit()
# 
# 
plt.ion()
fig = plt.figure()
plot = plt.imshow(display)
plt.pause(1)

# exit()

start = (0,0)
end = (len(data)-1, len(data[0])-1)
print(start, end)

def heuristic(pos):
    x,y = pos
    return abs(end[0] - x) + abs(end[1] - y)

#print(start, end, heuristic((0,0)))

def neighbours(pos):
    x,y = pos
    if x > 0:
        yield (x-1, y)
    if y > 0:
        yield (x, y-1)
    if x < end[0]:
        yield (x+1, y)
    if y < end[1]:
        yield (x, y+1)


def reconstruct(previous, point):
    pt = point
    reconstructed = [pt]
    while pt in previous:
        pt = previous[pt]
        reconstructed.append(pt)
    return reconstructed


def update_plot(previous, current, fscore):
    x,y=current

    # option 1
    chain = reconstruct(previous, current)
    for x,y in chain:
        display[x,y] = [1.,0.,0.3]

    # # option 2
    # chain = reconstruct(previous, current)
    # xs,ys = zip(*chain)
    # display[xs,ys] = [1.,0.,0.3]

    # # option 3
    # chain = np.array(reconstruct(previous, current))
    # display[chain[:,0],chain[:,1]] = [1.,0.,0.3]

    plot.set_data(display)
    plt.title(f"{len(chain)} steps. effort={fscore}")
    plt.draw()
    plt.pause(0.001)
    display[:] = display_orig


def update_for_neighbour(nb, h, gscore, data, previous, current, fringe):
    #print('neighbour',nb,end=' ')
    x,y = nb
    tmp_gscore = gscore[current] + data[x][y] 
    #print(tmp_gscore)
    if tmp_gscore < gscore[nb]:
        previous[nb] = current
        #print('pushing', nb)
        gscore[nb] = tmp_gscore
        hq.heappush(fringe, (tmp_gscore + h(nb), nb))


def astar(start, end, h):
    fringe = []
    hq.heappush(fringe, (h(start), start))
    gscore = ddict(lambda: 999999)
    gscore[start] = 0
    previous = {}

    ctr = 0
    while fringe:
        fscore, current = hq.heappop(fringe)
        if ctr % 1000 == 0 or current == end:
            update_plot(previous, current, fscore)
        ctr += 1
        #print('current',fscore,current)#,fringe)
        if current == end:
            return fscore, reconstruct(previous, end)
        for nb in neighbours(current):
            update_for_neighbour(nb, h, gscore, data, previous, current, fringe)
        #sleep(0.1)
    assert False, "no path"


print('Result:', astar(start, end, heuristic))
plt.ioff()
plt.show()
