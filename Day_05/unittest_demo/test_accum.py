import accum as m

nums = {
    (4,5) : 9,
    (6,2) : 8,
    ('a','b') : 'ab',
}



]


def test_add_two():
    assert m.add_two(4, 5) == 9




def test_always_fine():
    pass

def test_accumulator_exists():
    from accum import Accumulator

def test_accum_is_zero_at_start():
    acc = m.Accumulator()
    assert acc.sum == 0

def test_at_start_no_last_received():
    acc = m.Accumulator()
    assert acc.last_received is None

def test_one_add():
    acc = m.Accumulator()
    acc.add(4)
    results = [
        acc.sum == 4,
        acc.last_received == 4,
    ]
    assert all(results)
