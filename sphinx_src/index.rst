2022 NORBIS/DLN Software school
===============================

.. Course website at http://indico.ictp.it/event/8654/

The course repos are at https://git.app.uib.no/ii/collabschool22/,
including the `timetable <https://git.app.uib.no/ii/collabschool22/timetable>`_

.. Etherpad at https://etherpad.net/p/ictp2019

Only some of the exercise instructions are hosted here.

Exercises
---------

.. toctree::
   :maxdepth: 1

   landcover/index


Group Projects
--------------

.. toctree::
  :maxdepth: 1

  bikegenes/index
  chasinscores/index
  trafficjam/index   
  planets/index
  
..  crowds/index


..   version_control
..   project_euler
..   bash_dataexercise
